'use strict';

const themeButton = document.getElementById('themeChooser');
themeButton.addEventListener('click',theme_changer);

const plusButton = document.getElementById('plus');
plusButton.addEventListener('click',add);

const minusButton = document.getElementById('minus');
minusButton.addEventListener('click',sub);

function theme_changer() {
  const stylesheets = document.getElementsByTagName('link');
  for(let i = 0; i < stylesheets.length; i++) {
    if(stylesheets[i].href.includes('day.css')) {
      stylesheets[i].href = "./styles/night.css";
      break;
    }
    else if(stylesheets[i].href.includes('night.css')) {
      stylesheets[i].href = "./styles/day.css";
      break;
    }
  }

  const logos = document.querySelectorAll('.logo');

  for(let i=0; i < logos.length; i++) {
    let source = logos[i].src;
    source = name_finder(source);
    logos[i].src = './image/' + source;
  }
}

function name_finder(string) {
  let i = string.length-1;
  let dot = 0;
  let score = 0;
  while(string[i] != '/') {
    if(string[i] == '.') {
      dot = i;
    }
    if(string[i] == '-') {
      score = i;
    }
    i--;
  }
  i++;

  let name = '';
  if(score == 0) {
    name = string.slice(i,dot);
    name = name + '-inv.png';
  }
  else {
    name = string.slice(i,score);
    name = name + '.png';
  }
  return name;
}

function add() {
  const counter = document.getElementById('counter');
  counter.innerHTML = Number(counter.innerHTML) + 1;
}

function sub() {
  const counter = document.getElementById('counter');
  counter.innerHTML = Number(counter.innerHTML) - 1;
}